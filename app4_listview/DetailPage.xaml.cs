﻿using System;
using System.Collections.Generic;
using app4_listview.ViewModels;
using Xamarin.Forms;

namespace app4_listview
{
    public partial class DetailPage : ContentPage
    {

        public DetailPage(Workouts y)
        {
            
            InitializeComponent();
            BindingContext = new Workouts()
            {
                Title = y.Title,
                Img2 = y.Img2,
                Details = y.Details,
                Details2 = y.Details2,
                Img = y.Img,
                Subtitle = y.Subtitle
            };
            
        }
    }
}
