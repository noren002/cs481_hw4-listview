﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using app4_listview.ViewModels;
using Xamarin.Forms;

namespace app4_listview
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new Workouts();
            jakesList.ItemsSource = new List<Workouts>() {
                   new Workouts()
                    {
                        Img2 = "BenchDetailsImg.jpg",
                        Img = "benchpress.jpg",
                        Title = "Bench Press",
                        Subtitle = "An exercise in which a lifter lies on a bench with the feet on the floor and raises a weight with both arms."
                    },
                    new Workouts()
                    {
                        Img2 = "SquatDetailsImg.jpeg",  
                        Img = "squat.jpg",
                        Title = "Squat",
                        Subtitle = "Performed by bending deeply at the knees and then rising, especially with a barbell resting across the shoulders." },
                    new Workouts()
                    {
                        Img2 = "DeadLiftDetailsImg.jpg",  
                        Img = "deadlift.jpg",
                        Title = "Deadlift",
                        Subtitle = "An exercise in which a loaded barbell or bar is lifted off the ground to the level of the hips, torso perpendicular to the floor." },
                    new Workouts()
                    {
                        Img2 = "OverheadPressDetailsImg.jpg",  
                        Img = "overhead.png",
                        Title = "Shoulder Press",
                        Subtitle = "An exercise typically performed while standing, in which a weight is pressed straight upwards until the arms are locked out overhead." },
                    new Workouts()
                    {
                        Img2 = "RowDetailsImg.jpg",  
                        Img = "row.jpg",
                        Title = "Bent-Over Row",
                        Subtitle = "Bending over until the upper body is at a 45-degree bend or lower and pulling the bar up towards the lower chest." },
                    new Workouts()
                    {
                        Img2 = "BicepCurlDetailsImg.jpg",  
                        Img = "curl.jpg",
                        Title = "Bicep Curl",
                        Subtitle = "An exercise with the arm extended and then lifted to complete full flexion of the arm at the elbow." },
                    new Workouts()
                    {
                        Img2 = "TricepDetailsImg.png",  
                        Img = "tricep.jpg",
                        Title = "Tricep Extension",
                        Subtitle = "An exercise with the arm in full flexion and then extended fully at the elbow." },
                    new Workouts()
                    {      
                        Img2 = "SideShoulderRaiseDetailsImg.jpg",  
                        Img = "Sideimg.jpeg",
                        Title = "Side Lateral Raise",
                        Subtitle = "A shoulder exercise in which the arms are raised out to the sides." },
                    new Workouts()
                    {
                        Img2 = "LatPulldownDetailsImg.jpg",  
                        Img = "LatPullimg.jpeg",
                        Title = "Lateral Pull Down",
                        Subtitle = "A strength training exercise designed to develop the latissimus dorsi muscle." },
                    new Workouts()
                    {
                        Img2 = "ShrugDetailsImg.jpg",  
                        Img = "Shrugimg.jpg",
                        Title = "Shoulder Shrug",
                        Subtitle = "An exercise in weight training used to develop the upper trapezius muscle." },
                    new Workouts()
                    {
                        Img2 = "HamCurlDetailsImg.png",  
                        Img = "LegCurlimg.webp",
                        Title = "Hamstring Curl",
                        Subtitle = "An isolation exercise that targets the hamstring muscles, located in the rear of the upper leg." },
                    new Workouts()
                    {
                        Img2 = "QuadDetailsImg.jpg",  
                        Img = "LegExtimg.jpeg",
                        Title = "Quadricep Extension",
                        Subtitle = "An isolated exercise targeting one specific muscle group, the quadriceps located in the front of the upper leg. " },
                    new Workouts()
                    {
                        Img2 = "CalfRaiseDetailsImg.jpg",  
                        Img = "Calfimg.jpeg",
                        Title = "Calf Raise",
                        Subtitle = "A method of exercising the gastrocnemius, tibialis posterior and soleus muscles of the lower leg." },
                    new Workouts()
                    {
                        Img2 = "PullUpsDetailsImg.png",  
                        Img = "PullUpimg.jpeg",
                        Title = "Pull Ups",
                        Subtitle = "A simple and effective upper body pulling strength exercise that also can help with shoulder mobility." },
                    new Workouts()
                    {
                        Img2 = "PushUpsDetailsImg.webp",  
                        Img = "PushUpimg.jpeg",
                        Title = "Push Ups",
                        Subtitle = "A basic exercise used in civilian athletic training or physical education and commonly in military physical training" },
                    new Workouts()
                    {
                        Img2 = "AbDetailsImg.jpg",  
                        Img = "Abimg.jpeg",
                        Title = "Abdominal Crunch",
                        Subtitle = "An exercise with the lower back staying on the floor, you can effectively isolate the abdominals." }
            };

        }

        void Handle_Refreshing(object Sender, System.EventArgs e)
        {
            jakesList.IsRefreshing = false;
        }

        async void Handle_Details(object Sender, System.EventArgs e)
        {
            var x = (MenuItem)Sender;
            Workouts y = (Workouts)x.CommandParameter;
            await Navigation.PushAsync(new DetailPage(y));
        }

    }
}
